﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function:Manages all the players, controls, and ui
this will run through every scene.

****************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;


    public List<Material> highLighMaterials;

    public List<GameObject> players;


    public GamePadManager padManager;

    private HudManager hudManager;
    private RoundManager roundManager;


    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        //hides xbox curser 
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        // This object has to exist in all scenes
       // DontDestroyOnLoad(this);

        //Debug.Log(SceneManager.GetActiveScene().name);

        Debug.logger.logEnabled = false;


    }

    //loop to set everything the player needs(ui, hud, character,chest)
    private void GenerateLevel()
    {


        string playerTag = "Spawn_Player";
        string chestTag = "Spawn_Chest";



        if (players.Count > 0)
        {

            Vector3 location;
            GameObject player;
            Character character;
            GameObject chest;
            Chest chestScpt;

            

            if (hudManager == null)
            {
                hudManager = GameObject.FindGameObjectWithTag("HUDManager").GetComponent<HudManager>();

                roundManager = hudManager.GetComponent<RoundManager>();
            }

            // Instantiate Players
            for (int i = 0; i < players.Count; i++)
            {

                if (players[i] == null)
                {
                    continue;
                }

                // Find the position of the player spawn point
                location = GameObject.FindGameObjectWithTag(playerTag + (i + 1).ToString()).transform.position;

                player = Instantiate(players[i], location, new Quaternion()) as GameObject;

                character = player.GetComponent<Character>();

                // Add material color to the player's feet
                character.highLight.material = highLighMaterials[i];

                // Set tag to the players
                character.padTag = padManager.getPlayerTag(i + 1);
                character.playerIndex = (i + 1);

                player.tag = "Player" + (i + 1).ToString();


                // Find the position of the player's chest spawn point
                location = GameObject.FindGameObjectWithTag(chestTag + (i + 1).ToString()).transform.position;
                chest = Instantiate(character.chest, location, new Quaternion()) as GameObject;

                character.chest = chest;

                chestScpt = chest.GetComponent<Chest>();
                chestScpt.playerIndex = character.playerIndex;

                // Find the score of the players inside the hudmanager
                hudManager.AddPlayer(i, character.charModel);

                //Send script to activate and let the player receive input from gamepad
                roundManager.playerScript.Add(character);

                PlayerHUD.game_points = 0.0f;

                players[i] = null;


            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        //hide debug console
        Debug.developerConsoleVisible = false;


        switch (SceneManager.GetActiveScene().name)
        {

            case "Level":

                GenerateLevel();
                break;

            default:
                break;
        }


    }

    public void SetPlayer(int playerNum, GameObject charGO)//, Material highlightMaterial)
    {
        players[playerNum - 1] = charGO;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level");
    }


}
