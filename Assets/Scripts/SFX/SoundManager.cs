﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function:Sound manager to manage every sound
inside lists and call to use

****************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public enum SoundList
    {
        UI,
        PLAYER,
        AMBIENT,
        EMMI,
        BLISS,
        SAPHORA,
        YUI,
        TOTAL
    }

    [System.Serializable]
    public class SoundDict
    {
        public string key;
        public AudioClip value;
    }


    [Header("Ui Sounds")]
    public List<SoundDict> uiList;

    [Header("Player Sounds")]
    public List<SoundDict> playerList;

    [Header("Ambient Sounds")]
    public List<SoundDict> ambList;

    [Header("Emmi Sounds")]
    public List<SoundDict> emmiList;

    [Header("Bliss Sounds")]
    public List<SoundDict> blissList;

    [Header("Saphora Sounds")]
    public List<SoundDict> saphoraList;

    [Header("Yui Sounds")]
    public List<SoundDict> yuiList;

    //gets the specific clip from the list
    public AudioClip GetClip(string key, SoundList list)
    {

        AudioClip clip = null;

        List<SoundDict> sounds;

        switch (list)
        {
            case SoundList.UI:
                sounds = uiList;
                break;

            case SoundList.PLAYER:
                sounds = playerList;
                break;

            case SoundList.AMBIENT:
                sounds = ambList;
                break;

            case SoundList.EMMI:
                sounds = emmiList;
                break;

            case SoundList.BLISS:
                sounds = blissList;
                break;

            case SoundList.SAPHORA:
                sounds = saphoraList;
                break;

            case SoundList.YUI:
                sounds = yuiList;
                break;

            default:
                return null;
        }



        for (int i = 0; i < sounds.Count; i++)
        {

            if (key == sounds[i].key)
            {
                clip = sounds[i].value;
                break;
            }

        }

        return clip;

    }

    public static SoundManager instance;


    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        //DontDestroyOnLoad(this);
    }
}
