﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: sound for transitioning the menu to gameplay

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionSound : MonoBehaviour {

    private SoundManager sm;

    private void Start()
    {
        sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
    }

    private void OnDisable()
    {
        
    }
}
