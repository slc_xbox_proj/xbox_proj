﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function:Manages all the model in character selection
and winning screen

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharModel : MonoBehaviour {

    private Animator anim;
    private SoundManager soundManager;
    private AudioSource audioSource;

    public SoundManager.SoundList characterSoundList;

    public CharSelection character;

    public string charName;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        audioSource = Camera.main.GetComponent<AudioSource>();
        soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        charName = transform.name;

        //get the model name and store it, set list to the character
        if (SceneManager.GetActiveScene().name == "WinnerScreen")
        {
            if (charName.Contains("Emmi-Chan"))
            {
                characterSoundList = SoundManager.SoundList.EMMI;
            }

            if(charName.Contains("Bliss-Chan"))
            {
                characterSoundList = SoundManager.SoundList.EMMI;
            }

            if (charName.Contains("Saphora-Chan"))
            {
                characterSoundList = SoundManager.SoundList.SAPHORA;
            }

            if(charName.Contains("Yui-Chan"))
            {
                characterSoundList = SoundManager.SoundList.YUI;
            }

        }

    }
	
	// Update is called once per frame
	void Update () {

        //transition animations and play sounds
        if (SceneManager.GetActiveScene().name == "Menu")
        {

            if (character.selected == false)
            {
                gameObject.transform.Rotate(0, Time.deltaTime * 30, 0);
                if (anim.GetBool("Chosen"))
                {
                    anim.SetBool("Chosen", false);
                }
            }
            else
            {
                if (!anim.GetBool("Chosen"))
                {
                    gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
                    anim.SetBool("Chosen", true);
                    audioSource.PlayOneShot(soundManager.GetClip("pick", characterSoundList));
                }
            }
        }
        else if(SceneManager.GetActiveScene().name == "WinnerScreen")
        {

            if (!anim.GetBool("Winner"))
            {
                anim.SetBool("Winner", true);
                audioSource.PlayOneShot(soundManager.GetClip("winner", characterSoundList));
            }


        }
	}

    
    void OnEnable()
    {
        gameObject.transform.rotation = Quaternion.Euler(0,180,0);
    }


}
