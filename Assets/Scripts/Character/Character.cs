﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: All the character in game logic, stealing,
moving, commiting, steal, picking up, dropping.

****************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{


    const float EVENT_TIME = 1.5f;

    const float GAMEPAD_DEADZONE = 0.04f;



    public UnityEngine.UI.Text x_axis;
    public UnityEngine.UI.Text y_axis;
    public UnityEngine.UI.Text rotation;





    [System.Serializable]
    public class Movement
    {

        public float vertical;
        public float horizontal;

        public Vector3 direction;

        public Quaternion rotation;

        [Range(0.5f, 10.0f)]
        public float speed = 10.0f;

    };

    [System.Serializable]
    public class Boundary
    {
        public float x_min, x_max, y_min, y_max, z_min, z_max;
    };

    public Boundary boundary;

    public GameObject chest;

    private GameObject tempItem;

    private bool isCommiting_stealing = false;
    private bool inChestEvent = false;
    private float eventTimeStart = 0;
    private float eventTimeEnd = 0;
    private GameObject eventChest = null;

    public Animator anim;


    public MeshRenderer highLight;


    protected CharacterController controller;
    private HudManager hudManager;
    private SoundManager soundManager;
    private AudioSource audioSource;




    private bool knockedOut = false;

    private bool abilityOn = false;

    protected int maxItens = 1;
    List<GameObject> bag;


    public Movement movement = new Movement();

    public int playerIndex;
    public string padTag;


    public GameObject charModel;



    protected abstract void StartChild();

    private void Start()
    {

        StartChild();

        controller = GetComponent<CharacterController>();
        hudManager = GameObject.FindGameObjectWithTag("HUDManager").GetComponent<HudManager>();
        soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        audioSource = Camera.main.GetComponent<AudioSource>();

        if (controller == null)
        {
            Debug.Log("No CharacterController attached", this);
        }



        //Debug.Log(Input.GetJoystickNames());

        bag = new List<GameObject>(maxItens);


        //SpawnChest();

        //Get the animation controller from the character
        anim = GetComponent<Animator>();


    }

    //private void SpawnChest()
    //{

    //}


    void Update()
    {

        //try
        //{


            if (padTag != "")
            {



                // Character can only move if he`s not knocked out
                if (!knockedOut)
                {

                    float haxis = 0.0f;
                    float vaxis = 0.0f;

                    haxis = Input.GetAxis(padTag + " Horizontal");
                    vaxis = Input.GetAxis(padTag + " Vertical");

                    if (haxis < GAMEPAD_DEADZONE && haxis > -GAMEPAD_DEADZONE)
                        movement.horizontal = 0.0f;
                    else
                        movement.horizontal = haxis;

                    if (vaxis < GAMEPAD_DEADZONE && vaxis > -GAMEPAD_DEADZONE)
                        movement.vertical = 0.0f;
                    else
                        movement.vertical = vaxis;

                    movement.direction = new Vector3(movement.horizontal * movement.speed * Time.deltaTime,
                                                    0.0f,
                                                    movement.vertical * movement.speed * Time.deltaTime);

                    SetBoundary();

                    controller.Move(movement.direction);

                    // Rotate the player according to the player`s movement
                    if (movement.direction != Vector3.zero)
                    {

                        if (!anim.GetBool("isRunning"))
                        {
                            anim.SetBool("isRunning", true);
                        }



                        transform.rotation = Quaternion.LookRotation(movement.direction);
                    }
                    else
                    {

                        if (anim.GetBool("isRunning"))
                        {
                            anim.SetBool("isRunning", false);
                        }

                    }

                }





                //check if the joystick axis is moving, then play animation
                ////    if (movement.vertical > 0.4 || movement.vertical < -0.4 ||
                ////        movement.horizontal > 0.4 || movement.horizontal < -0.4)
                ////    {
                ////        if (anim.GetBool("isRunning") != true)
                ////        {
                ////            anim.SetBool("isRunning", true);
                ////        }

                ////    }
                ////    else
                ////    {
                ////        if (anim.GetBool("isRunning") != false)
                ////        {
                ////            anim.SetBool("isRunning", false);
                ////        }
                ////    }

            }



            // Commit / Push
            if (inChestEvent)
            {

                if (Input.GetButtonDown(padTag + " PickPushItem"))
                {

                    if (eventChest == chest && bag.Count > 0)
                    {

                        int last = bag.Count - 1;

                        // Get the last item on bag
                        GameObject item = bag[last];

                        //item.GetComponent<Item>().Drop(transform.position);

                        // Remove item from bag
                        bag.RemoveAt(last);

                        //play audio
                        audioSource.PlayOneShot(soundManager.GetClip("drop", SoundManager.SoundList.PLAYER));

                        Chest chestScpt = eventChest.GetComponent<Chest>();

                        chestScpt.Push(item);
                        hudManager.ChestPush(playerIndex, chestScpt.GetChestScore());


                    }

                }

                if (Input.GetButtonDown(padTag + " CommitStealItem"))
                {

                    if (eventChest.GetComponent<Chest>().GetChestScore() > 0 && !isCommiting_stealing)
                    {
                        Debug.Log("Commit");
                        StartCountDown();
                        isCommiting_stealing = true;

                        if (anim.GetBool("Commit") != true)
                        {
                            anim.SetBool("Commit", true);
                        }
                    }
                }

                else if (Input.GetButtonUp(padTag + " CommitStealItem"))
                {

                    StopCountDown();
                    isCommiting_stealing = false;
                    if (anim.GetBool("Commit") != false)
                    {
                        anim.SetBool("Commit", false);
                    }

                }

                if (isCommiting_stealing && Time.time >= eventTimeEnd)
                {

                    eventTimeStart = eventTimeEnd = 0;
                    //inChestEvent = false;
                    isCommiting_stealing = false;

                    if (anim.GetBool("Commit") != false)
                    {
                        anim.SetBool("Commit", false);
                    }

                    audioSource.PlayOneShot(soundManager.GetClip("commit", SoundManager.SoundList.PLAYER));

                    Chest chestScpt = eventChest.GetComponent<Chest>();



                    if (eventChest == chest)
                    {
                        float points = chestScpt.Commit();

                        hudManager.ChestCommit(playerIndex, points);
                    }
                    else
                    {
                        GameObject item = chestScpt.Pop();

                        if (!CarryItem(item))
                        {
                            chestScpt.Push(item);
                        }
                        else
                        {
                            hudManager.ChestPop(chestScpt.playerIndex, chestScpt.GetChestScore());
                        }


                    }



                }
                else
                {
                    //canvas.ShowBuyProgress((Time.time - eventTimeStart) / (TIME_TO_BUY / 100) / 100);
                }
            }
            else
            {
                if (anim.GetBool("Commit") != false)
                {
                    anim.SetBool("Commit", false);
                }

                if (Input.GetButtonDown(padTag + " PickPushItem"))
                {

                    // Check whether there`s an Item to carry
                    if (tempItem != null)
                    {
                        // If player can carry the item, clear the item from temporary memory
                        if (CarryItem(tempItem))
                        {
                            tempItem = null;
                            audioSource.PlayOneShot(soundManager.GetClip("pick", SoundManager.SoundList.PLAYER));
                        }
                    }
                }
                else if (Input.GetButtonDown(padTag + " DropItem"))
                {

                    DropItem();

                }

            }




        //}
        //catch (Exception e)
        //{

        //    Debug.Log(e.Message);

        //}


    }


    private void StartCountDown()
    {

        eventTimeStart = Time.time;
        eventTimeEnd = eventTimeStart + EVENT_TIME;

    }

    private void StopCountDown()
    {

        eventTimeStart = eventTimeEnd = 0;
        //inChestEvent = false;
    }

    private void DropItem()
    {

        // Check whether there`s an Item to carry
        if (bag.Count > 0)
        {
            int last = bag.Count - 1;

            // Get the last item on bag
            GameObject item = bag[last];

            item.GetComponent<Item>().Drop(transform.position);

            // Remove item from bag
            bag.RemoveAt(last);

            audioSource.PlayOneShot(soundManager.GetClip("drop", SoundManager.SoundList.PLAYER));
        }

    }


    // FixedUpdate is called once per frame
    //void FixedUpdate()
    //{
    //    SetBoundary();
    //    // Character can only move if he`s not knocked out
    //    if (!knockedOut)
    //    {

    //        controller.Move(movement.direction);

    //        // Rotate the player according to the player`s movement
    //        if (movement.direction != Vector3.zero)
    //        {
    //            transform.rotation = Quaternion.LookRotation(movement.direction);
    //        }

    //    }

    //}


    void ToggleTooltip()
    {
        Chest chestScrpt = eventChest.GetComponent<Chest>();

        if (eventChest != null)
        {
            if (eventChest == chest && chestScrpt.enemyInfo.activeSelf == false)
            {
                chestScrpt.ToggleInformation(chestScrpt.playerInfo);
            }
            else if (chestScrpt.playerInfo.activeSelf == false)
            {
                chestScrpt.ToggleInformation(chestScrpt.enemyInfo);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {


        switch (other.tag)
        {
            case "Item":
                tempItem = other.gameObject;
                break;

            case "Chest":

                //if (other.gameObject == chest)
                //{
                //    mychest = true;
                //}
                //else
                //{
                //    otherChest = other.gameObject;
                //}

                inChestEvent = true;
                eventChest = other.gameObject;
                ToggleTooltip();




                break;

            default:
                break;
        }


    }


    void OnTriggerExit(Collider other)
    {

        switch (other.tag)
        {
            case "Item":
                tempItem = null;
                break;

            case "Chest":

                //if (other.gameObject == chest)
                //{
                //    mychest = false;
                //}
                //else
                //{
                //    otherChest = null;
                //}
                inChestEvent = false;
                ToggleTooltip();
                eventChest = null;

                break;

            default:
                break;
        }

    }





    public bool CarryItem(GameObject item)
    {

        bool ret = false;

        // If player is not carrying enough itens, add the item to bag
        if (bag.Count < maxItens)
        {
            item.GetComponent<Item>().Pick();

            bag.Add(item);

            ret = true;
        }

        return ret;

    }

    //manages boundaries of the level so the player cannot exit
    public void SetBoundary()
    {
        transform.position = new Vector3
        (
            Mathf.Clamp(transform.position.x, boundary.x_min, boundary.x_max),
            Mathf.Clamp(transform.position.y, boundary.y_min, boundary.y_max),
            Mathf.Clamp(transform.position.z, boundary.z_min, boundary.z_max)
        );
    }

}
