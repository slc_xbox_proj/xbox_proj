﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Set Xbox Input 

****************************************************/

using UnityEngine;
using UnityEngine.EventSystems;

public class XboxDPadInput : MonoBehaviour
{
    private GameObject currentButton;
    private AxisEventData currentAxis;
    //timer
    private float timeBetweenInputs = 0.0f; //in seconds
    private float timer = 0;

    void Update()
    {
        //if (timer <= 0)
        //{
            currentAxis = new AxisEventData(EventSystem.current);
            currentButton = EventSystem.current.currentSelectedGameObject;

            if (Input.GetAxis("VerticalDpad") > 0) // move up
            {
                currentAxis.moveDir = MoveDirection.Up;
                ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
                timer = timeBetweenInputs;
            }
            else if (Input.GetAxis("VerticalDpad") < 0) // move down
            {
                currentAxis.moveDir = MoveDirection.Down;
                ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
                timer = timeBetweenInputs;
            }
            else if (Input.GetAxis("HorizontalDpad") > 0) // move right
            {
                currentAxis.moveDir = MoveDirection.Right;
                ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
                timer = timeBetweenInputs;
            }
            else if (Input.GetAxis("HorizontalDpad") < 0) // move left
            {
                currentAxis.moveDir = MoveDirection.Left;
                ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
                timer = timeBetweenInputs;
            }
       // }

        //timer counting down
       // if (timer > 0) { timer -= Time.deltaTime; } else { timer = 0; }
    }
}