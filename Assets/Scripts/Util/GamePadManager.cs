﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Assign A player to a specific joystick, give 
players tag

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePadManager : MonoBehaviour {

    public static GamePadManager instance;


    [System.Serializable]
    public class PlayerGamePadMap
    {

        const int MAX_PLAYERS = 11;

        string[] playerTag = { "", "", "", "" };

        int index = 0;

        
        public bool AddPlayer(string tag)
        {

            // Check if there`s still availabe space for new players
            if (index < MAX_PLAYERS)
            {

                // Check for repeated entry 
                for (int i = 0; i < index; i++)
                {
                    if (tag == playerTag[i])
                    {
                        return false;
                    }
                }

                // If there`s no repeated entry, add player
                playerTag[index] = tag;
                index++;

                return true;
            }else
            {
                return false;
            }
        }



        //get the player tag
        public string getPlayer(int num)
        {

            if (num <= index)
                return playerTag[num - 1];
            else
                return null;
        }

        //clear all tags
        public void Clear()
        {
            index = 0;

            for (int i = 0; i < 4; i++)
            {
                playerTag[i] = "";
            }

        }

    }


    // Players Tag
    public PlayerGamePadMap playerMap = new PlayerGamePadMap();


    private bool mapping = true;


    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    // Use this for initialization
    void Start () {

        //DontDestroyOnLoad(this);

    }

    // Update is called once per frame
    void Update()
    {

        // Check if Manager is mapping gamepads
        if (mapping)
        {

            for (int i = 1; i <= 4; i++)
            {
                foreach (int map in System.Enum.GetValues(typeof(GAMEPAD_XBONE_BUTTONS)))
                {
                    string joystick = "joystick " + i.ToString();
                    string button = " button " + map;

                    bool test = Input.GetKeyDown(joystick + button);

                    if (test)
                        Debug.Log(playerMap.AddPlayer(joystick));

                }
            }
        }
    }



    //void OnGUI()
    //{
    //    int w = Screen.width, h = Screen.height;

    //    GUIStyle style = new GUIStyle();

    //    Rect rect = new Rect(0, 0, w, h * 6 / 100);
    //    style.alignment = TextAnchor.UpperLeft;
    //    style.fontSize = h * 6 / 100;
    //    style.normal.textColor = new Color(1.0f, 1.0f, 0.0f, 1.0f);
    //    float msec = Time.deltaTime * 1000.0f;
    //    float fps = 1.0f / Time.deltaTime;
    //    string text = playerMap.getPlayer(1);
    //    GUI.Label(rect, text, style);
    //}


    public void ToggleMapping(bool boolean)
    {
        mapping = boolean;
    }

    public string getPlayerTag(int playerNum)
    {

        return playerMap.getPlayer(playerNum);


    }
}
