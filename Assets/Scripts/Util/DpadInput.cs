﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Xbox One Mapping

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DpadInput : MonoBehaviour {

    private GameObject currentButton;
    private AxisEventData currentAxis;
    //timer
    private float timeBetweenInputs = 0.0f; //in seconds
    private float timer = 0;

	
	// Update is called once per frame
	void Update () {

        currentAxis = new AxisEventData(EventSystem.current);
        currentButton = EventSystem.current.currentSelectedGameObject;

        if (Input.GetAxis("VerticalDpad") > 0) // move up
        {
            currentAxis.moveDir = MoveDirection.Up;
            ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
            timer = timeBetweenInputs;
        }
        else if (Input.GetAxis("VerticalDpad") < 0) // move down
        {
            currentAxis.moveDir = MoveDirection.Down;
            ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
            timer = timeBetweenInputs;
        }
        else if (Input.GetAxis("HorizontalDpad") > 0) // move right
        {
            currentAxis.moveDir = MoveDirection.Right;
            ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
            timer = timeBetweenInputs;
        }
        else if (Input.GetAxis("HorizontalDpad") < 0) // move left
        {
            currentAxis.moveDir = MoveDirection.Left;
            ExecuteEvents.Execute(currentButton, currentAxis, ExecuteEvents.moveHandler);
            timer = timeBetweenInputs;
        }

    }
}
