﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Items logic, despawn when pick up add
a specific value for the points
spawn tooltip for player when they are able to pick
up the item

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {


    public GameObject playerInfo;

    public float value;
 

    private bool canBePicked = false;
    private int collisionsCount = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    //entering the trigger show tooltip
    private void OnTriggerEnter(Collider other)
    {

        //if (other.gameObject.tag == "Player")
        //{
            playerInfo.SetActive(true);

            //canBePicked = true;
            //collisionsCount++;
        //}

    }

    //leaving the trigger despawn tooltip
    private void OnTriggerExit(Collider other)
    {
        //if (other.gameObject.tag == "Player")
        //{
            playerInfo.SetActive(false);

            //collisionsCount--;

            //if (collisionsCount == 0)
            //    canBePicked = false;

        //}
    }


    //drop the item back on the floor setting it active in the position it is dropped
    public void Drop( Vector3 location )
    {

        transform.position = location;
        
        gameObject.SetActive(true);

    }


    //pick up the game item, set inactive
    public void Pick()
    {

       gameObject.SetActive(false);

    }

}
