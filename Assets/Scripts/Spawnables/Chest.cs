﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: chest logic (add items, score etc)

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    public GameObject playerInfo;
    public GameObject enemyInfo;

    class MostValuableItem
    {
        public int index;
        public float value;

        public MostValuableItem()
        {
            index = 0;
            value = 0.0f;
        }

        //clear whats in the chest
        public void Clear()
        {
            index = 0;
            value = 0;
        }
    }

    //toggle the tooltip upon entry
    public void ToggleInformation(GameObject Tooltip)
    {
            if (Tooltip.activeSelf == false)
            {
                Tooltip.SetActive(true);
            }
            else
            {
                Tooltip.SetActive(false);
            }  
    }

    public int playerIndex;

    // List of Itens being held within the chest
    private List<GameObject> items = new List<GameObject>();

    // Most valuable item inside the Chest
    private MostValuableItem mostValuableItem = new MostValuableItem();

    // Total amount of the Itens value
    private float points = 0.0f;

    private int total_items = 0;

    



	// Use this for initialization
	void Start () {
        //on new game clear all chest items
        items.Clear();
    }
	
	// Update is called once per frame
	//void Update () {
		
	//}


    // Add an item to the chest
    public void Push(GameObject itemGO)
    {

        Item item = itemGO.GetComponent<Item>();

        // Add item to the chest`s list
        items.Add(itemGO);

        // If the chest is empty, this is the most valuable item.
        if (mostValuableItem.value == 0)
        {
            mostValuableItem.value = item.value;
            mostValuableItem.index = 0;
        }
        else
        {

            // If it has a higher value, it is now the most valuable item
            if(item.value > mostValuableItem.value)
            {

                mostValuableItem.value = item.value;
                mostValuableItem.index = items.Count - 1;
            }

        }

        // Update chest info
        points += item.GetComponent<Item>().value;
        total_items++;

    }

    // Remove an item from the chest
    public GameObject Pop()
    {

        GameObject _item;

        // Get the most valuable item
        _item = items[mostValuableItem.index];

        // Remove the item from the list
        items.RemoveAt(mostValuableItem.index);


        //Update chest info
        points -= mostValuableItem.value;
        total_items--;



        // Reset and lookk for the next most valuable item
        mostValuableItem.Clear();

        for (int i = 0; i < items.Count; i++)
        {
            Item item = items[i].GetComponent<Item>();

            if(item.value > mostValuableItem.value)
            {
                mostValuableItem.index = i;
                mostValuableItem.value = item.value;
            }
        }
        
        return _item;
    }


    // Turn the amount in the chest into player`s score
    public float Commit()
    {

        float total_points = points * total_items;

        // Clear List
        items.Clear();

        // Clear chest items info
        points = total_items = 0;

        // Clear items value info
        mostValuableItem.Clear();

        return total_points;

    }

    public float GetChestScore()
    {
        return total_items * points;
    }

}
