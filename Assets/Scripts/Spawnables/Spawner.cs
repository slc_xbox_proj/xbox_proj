﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Spawns all items at a set time

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] spawnable;
    public GameObject[] spawnPoint;
    private GameObject spawnLocation;

    private int index;
    public int totalSpawn;

    public float spawnDelay;
    public float waveDelay;


    void Start()
    {
        //start the spawn 
        StartCoroutine(SpawnItems());
    }

    //spawns a set amount of items every certain seconds at spawn locations
    IEnumerator SpawnItems()
    {
        index = Random.Range(0, spawnPoint.Length);
        spawnLocation = spawnPoint[index];
        yield return new WaitForSeconds(4);

        while (!RoundManager.GAMEOVER)
        {
            //loop in randoom increments between 1 and 3 (4 is exclusive)
            for (int i = 0; i < totalSpawn; i+= Random.Range(1,4))
            {
                index = Random.Range(0, spawnPoint.Length);

                //keep the local rotation of the object, and position of the y
                Quaternion spawnRotation = spawnable[i].transform.localRotation;
                Vector3 spawnPos = new Vector3(spawnLocation.transform.position.x, spawnable[i].transform.localPosition.y, spawnLocation.transform.position.z);

                Instantiate(spawnable[i], spawnPos, spawnRotation);
                spawnLocation = spawnPoint[index];
                

                yield return new WaitForSeconds(spawnDelay);

            }
            yield return new WaitForSeconds(waveDelay);
        }
    }
}
