﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function:Xbox One Mapping

****************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

enum GAMEPAD_XBONE_BUTTONS
{

    A = 0,
    B = 1,
    X = 2,
    Y = 3,
    LEFT_BUMBER = 4,
    RIGHT_BUMPER = 5,
    VIEW_BACK = 6,
    MENU_START = 7,
    LEFT_STICK = 8,
    RIGHT_STICK = 9,


}
