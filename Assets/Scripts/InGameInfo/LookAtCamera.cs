﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Have the game object look at the camera

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {


	// Use this for initialization
	void Start () {
        transform.LookAt(Camera.main.transform.position);
        transform.Rotate(0,180,0);
	}
	
}
