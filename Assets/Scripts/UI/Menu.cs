﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Manages all the menu functions
and transitions

****************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public GameObject eventSystem;

    public Menu next;

    public Menu prior;

    public GameObject menuPanel;

    private Animator animator;

    private SoundManager soundManager;

    private AudioSource uiAudioSource;

    //public bool isOpen
    //{
    //    get
    //    {
    //        return animator.GetBool("isOpen");
    //    }

    //    set
    //    {
    //        animator.SetBool("isOpen", value);
    //    }
    //}


    // Use this for initialization
    void Start () {

        animator = GetComponent<Animator>();

        soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        uiAudioSource = Camera.main.GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {

        //if (animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
        //{

        //}else
        //{

        //}

		
	}

    //pass in desired panel you wish to transition to from pause menu
    public void ToggleToPanel(GameObject panel)
    {
        menuPanel.SetActive(false);
        panel.SetActive(true);
    }

    //pass in the desired panel you wish to toggle off
    public void Back(GameObject panel)
    {
        panel.SetActive(false);
        menuPanel.SetActive(true);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void MoveNext()
    {

        this.gameObject.SetActive(false);

        //if (this.eventSystem)
        //    this.eventSystem.SetActive(false);



        next.gameObject.SetActive(true);

        uiAudioSource.PlayOneShot(soundManager.GetClip("TRANSITION", SoundManager.SoundList.UI));



        //if (next.eventSystem)
        //    next.eventSystem.SetActive(true);

    }


    //public void SetInteractable(bool toggle)
    //{

    //    gameObject.GetComponent<CanvasGroup>().interactable = toggle;


    //}


    public void GoToMainMenu()
    {


        GamePadManager pad = GameObject.FindGameObjectWithTag("GamePadManager").GetComponent<GamePadManager>();

        if (pad != null)
        {
            pad.playerMap.Clear();
        }


        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");

    }


}
