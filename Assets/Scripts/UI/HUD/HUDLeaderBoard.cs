﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Manages the scores and end results of match

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDLeaderBoard : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        



		
	}

    [System.Serializable]
    public class LeaderboardTexts
    {
        public GameObject panel;
        public Text name;
        public Text score;
    }


    public List<LeaderboardTexts> texts;


    public Text winnerName;
    public Text winnerScore;

    public Text runnerUpName;
    public Text runnerUpScore;

    public Text thirdName;
    public Text thirdScore;

    public Text fourthName;
    public Text fourthScore;



}
