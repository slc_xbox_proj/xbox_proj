﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Stops time when game is paused, and 
gives all logic for pause buttons to call

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{

    [HideInInspector]
    public static bool paused;

    private bool tooltipOpen;

    private HudManager hud;

    //Toggles the pause menu and changes the time scale, this will freeze/unfreeze gameplay
    public void TogglePause()
    {
        if (!paused)
        {
            Time.timeScale = 0;
            hud.panel.PauseMenu.SetActive(true);
            paused = true;
        }
        else if (paused)
        {
            Time.timeScale = 1;
            hud.panel.PauseMenu.SetActive(false);
            paused = false;
        }
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        hud.panel.PauseMenu.SetActive(false);
        paused = false;
    }

    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void ReturnToCharacterSelection()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Character Selection");
    }

    public void RestartMatch(string LevelName)
    {
        /*
         * add code for
         * reset player score
         * reset ability cooldowns
         */
        Time.timeScale = 1;
        SceneManager.LoadScene(LevelName);
    }

    //pass in desired panel you wish to transition to from pause menu
    public void ToggleToPanel(GameObject panel)
    {
        hud.panel.PauseMenu.SetActive(false);
        panel.SetActive(true);
        tooltipOpen = true;
    }

    //pass in the desired panel you wish to toggle off
    public void Back(GameObject panel)
    {
        panel.SetActive(false);
        hud.panel.PauseMenu.SetActive(true);
        tooltipOpen = false;
    }

    // Use this for initialization
    void Start()
    {
        hud = GetComponent<HudManager>();
        paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!tooltipOpen)
        {
            if (Input.GetButtonDown("Pause"))
            {
                TogglePause();
            }
        }
        //Keyboard Input
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    TogglePause();
        //}
    }
}
