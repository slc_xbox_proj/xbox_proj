﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Timer logic and decides when the game ends 
and starts

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RoundManager : MonoBehaviour {
    
    //control when the game ends
    [HideInInspector]public static bool GAMEOVER;
    [HideInInspector]public bool start;

    private HudManager hud;

    private int currentRound;
    private float roundTime;
    private float minutes;
    private float seconds;

    public List<Character> playerScript;

    private GameManager gm;

    //Set the time of the round according to mode
    public float setRoundTime(float ModeTime)
	{
		return roundTime = ModeTime;
	}
		
	public void updateTimer ()
	{
		//tick
		roundTime -= Time.deltaTime;

        minutes = Mathf.Floor(roundTime / 60);
        seconds = (roundTime % 60);

        //prevents the display of 60 seconds
        if (seconds > 59)
        {
            seconds = 59;
        }

        //check for the end of the round
        if (minutes < 0)
        {
            minutes = 0;
            seconds = 0;
            GAMEOVER = true;
        }

        //render in the proper format
        hud.text.RoundTimer.text = string.Format("{0:0}:{1:00}", minutes, seconds);
    }

   
    private IEnumerator StartCountdown()
    {

        yield return new WaitForSeconds(4);
        start = true;
        hud.icon.Countdown.gameObject.SetActive(false);


        // deactivate script to start gamepad input
        for (int i = 0; i < playerScript.Count; i++)
        {    
            playerScript[i].enabled = true;
        }

    }

    //private IEnumerator EndRound(string LevelName)
    //{
    //    yield return new WaitForSeconds(5);

    //    if (currentRound > 0)
    //    {
    //        SceneManager.LoadScene(LevelName);
    //    }
    //    else
    //    {
    //        SceneManager.LoadScene("WinnerScreen");
    //    }
    //}

    private IEnumerator EndGame()
    {
        // deactivate script to stop gamepad input
        for (int i = 0; i < playerScript.Count; i++)
        {
            playerScript[i].anim.SetBool("isRunning", false);
            playerScript[i].anim.SetBool("Commit", false);
            playerScript[i].enabled = false;
        }


        yield return new WaitForSeconds(5);

        SceneManager.LoadScene("WinnerScreen");
    }

    // Use this for initialization
    void Start () {
        hud = GetComponent<HudManager>();

        start = false;
        GAMEOVER = false;

        //roundTime = setRoundTime(120.0f);
        gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        

        roundTime = setRoundTime(90.0f);
        StartCoroutine(StartCountdown());
    }

	// Update is called once per frame
	void Update () {

		if (start && !GAMEOVER) {
			updateTimer();
		}
		else if(GAMEOVER) 
		{

            Debug.Log("GameOver");

            SetPlacements leaderboard = GameObject.FindGameObjectWithTag("Leaderboard").GetComponent<SetPlacements>();

            if (leaderboard != null)
            {

                List<PlayerHUD> playerHUD = GameObject.FindGameObjectWithTag("HUDManager").GetComponent<HudManager>().playerHUD;

                for (int i = 0; i < playerHUD.Count; i++)
                {

                    if (playerHUD[i] == null)
                    {
                        continue;
                    }

                    leaderboard.AddPlayerPlacement("Player " + (i + 1).ToString(), playerHUD[i].total_points, playerHUD[i].charModel);


                }


                hud.text.RoundTimer.text = string.Format("Game!");
                StartCoroutine("EndGame");

                this.enabled = false;
            }
            else
            {
                Debug.Log("Null object: ", leaderboard);
            }

		}
	}
}
