﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Manages all the gameplay ui, adjusts all the
scores and sliders accordingly

****************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour {

    [System.Serializable]
    public class Panels
    {
        public GameObject Player;
        public GameObject Round;
        public GameObject PauseMenu;
        public GameObject Controls;
        public GameObject[] PlayerStats;
    }

    [System.Serializable]
    public class Texts
    {
        //public Text[] PlayerScore;
        public Text[] ChestScore;
        public Text RoundTimer;
    }

    [System.Serializable]
    public class Icons
    {
        public Image[] PlayerIcon;
        public Image Countdown;
        //public Image[] ChestIcon;
    }

    [System.Serializable]
    public class ScoreBar
    {
        public Slider[] PlayerPoints;
    }

//    [System.Serializable]
//    public class PlayerHUD
//    {

//        public static float game_points;

//        public Image icon;
//        public Text chestScore;
//        public Slider slider;
//        public GameObject panel;

//        public float total_points;



//        public PlayerHUD(Image _icon, Slider _slider, Text _chestScore, GameObject _panel)
//        {

//            icon = _icon;
//            slider = _slider;
//            chestScore = _chestScore;
//            panel = _panel;

//            panel.SetActive(true);
//        }



//        public void AddChestScore(float _points)
//        {

//            //float points = float.Parse(chestScore.text) + _points;
//            chestScore.text = _points.ToString();

////            game_points += points;

//        }

//        public void ClearChestScore()
//        {
//            chestScore.text = "0";
//        }



//    }

    public Panels panel;
    public Texts text;
    public Icons icon;
    public ScoreBar scoreBar;

    //public static float totalScore;
    public List<PlayerHUD> playerHUD = new List<PlayerHUD>(4);

    private GameManager gm;

    //public void SumOfScore()
    //{
    //    for(int i = 0; i < gm.players.Count;)
    //    {
    //        totalScore += pScore[i].score;
    //    }
    //}

    //public void UpdateScore(int playerNum)
    //{
    //    scoreBar.PlayerPoints[playerNum].transform.localScale
    //        = new Vector3(score[playerNum].player / PlayerScore.total, scoreBar.PlayerPoints[playerNum].transform.localScale.y);
    //}

    //removes score from chest text
    public void ChestPop(int playerNum, float itemValue)
    {

  

       // playerHUD[playerNum - 1].AddChestScore(-itemValue);
        playerHUD[playerNum - 1].AddChestScore(itemValue);

    }

    //adds the score to the chest score text
    public void ChestPush(int playerNum, float itemValue)
    {
       

        playerHUD[playerNum - 1].AddChestScore(itemValue);
    }

    public void ChestCommit(int playerNum, float points)
    {
        // Add points to the player
        playerHUD[playerNum - 1].total_points += points;

        // Clear the chest points
        playerHUD[playerNum - 1].ClearChestScore();

        // Add total game points
        PlayerHUD.game_points += points;

        // Update slider
        UpdateSlider();



    }

    //updates all the sliders when commited
    private void UpdateSlider()
    {
        for(int i = 0; i < playerHUD.Count; i++)
        {
            playerHUD[i].slider.transform.localScale = new Vector3(playerHUD[i].total_points / PlayerHUD.game_points, playerHUD[i].slider.transform.localScale.y);
        }
    }

    private void Start()
    {
        gm = GetComponent<GameManager>();    
    }

    //public void UpdateChestScore(string player, float score)
    //{

    //    switch (player)
    //    {
    //        case "Player":

    //        default:
    //            break;
    //    }

    //}


        //add a panel for every player chosen
    public void AddPlayer(int playerIndex, GameObject charModel) {

        playerHUD.Insert(playerIndex, new PlayerHUD( icon.PlayerIcon[playerIndex], scoreBar.PlayerPoints[playerIndex], text.ChestScore[playerIndex], panel.PlayerStats[playerIndex], charModel));

    }

}
