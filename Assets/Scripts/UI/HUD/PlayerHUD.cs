﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Manages all the player hud

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//public class PlayerHUD : MonoBehaviour {

//	// Use this for initialization
//	void Start () {

//	}

//	// Update is called once per frame
//	void Update () {

//	}
//}

public class PlayerHUD
{


    public static float game_points;

    public Image icon;
    public Text chestScore;
    public Slider slider;
    public GameObject panel;

    public float total_points;


    public GameObject charModel;



    public PlayerHUD(Image _icon, Slider _slider, Text _chestScore, GameObject _panel, GameObject _model)
    {

        icon = _icon;
        slider = _slider;
        chestScore = _chestScore;
        panel = _panel;
        charModel = _model;

        panel.SetActive(true);
    }



    public void AddChestScore(float _points)
    {

        //float points = float.Parse(chestScore.text) + _points;
        chestScore.text = _points.ToString();

        //            game_points += points;

    }

    public void ClearChestScore()
    {
        chestScore.text = "0";
    }


}
