﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Has the player join the game in the character
selection screen

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinGame : MonoBehaviour {

    private GamePadManager gamePadManager;
    public GameObject charPanel;
    public GameObject camPanel;

    public CharSelectionCheck charSelectionCheck;

    [Range(1, 4)]
    public int playerNum;

    private string playerTag;

    // Use this for initialization
    void Start () {

        gamePadManager = GameObject.FindGameObjectWithTag("GamePadManager").GetComponent<GamePadManager>();


    }
	
	// Update is called once per frame
	void Update () {


        // Check if the player is not set
        if (playerTag == null)
        {

            // Get the player
            playerTag = gamePadManager.getPlayerTag(playerNum);

            // No Tag, Assume that there`s no player to select
            //selected = true;

            // Got the player. Remove panel
            if (playerTag != null)
            {
                charPanel.SetActive(true);
                camPanel.SetActive(true);

                charPanel.GetComponent<CharSelection>().playerNum = playerNum;
                charPanel.GetComponent<CharSelection>().playerTag = playerTag;

                

                gameObject.SetActive(false);

                //charImage.sprite = charEntity.getCurrent().image;

                charSelectionCheck.enabled = true;
            }

        }


    }
}
