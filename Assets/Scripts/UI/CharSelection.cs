﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Toggles models on and off and enables players
to lock in choose their character and start the game

****************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharSelection : MonoBehaviour
{

    //static int x;

    [System.Serializable]
    public class CharacterEntity
    {
        //public Sprite image;
        public GameObject model;
        public GameObject charGO;
    }

    [System.Serializable]
    public class CharacterList
    {

        public List<CharacterEntity> character;

        private int current = 0;

        // Return the current in the stack
        public CharacterEntity GetCurrent()
        {

            return character[current];

        }

        // Return the next image in the stack
        public CharacterEntity GetNext()
        {
            current++;

            if (current == character.Count)
            {
                current = 0;
            }

            return GetCurrent();

        }

        // Return the prior image in the stack
        public CharacterEntity GetPrior()
        {
            current--;

            if (current == -1)
            {
                current = character.Count - 1;
            }

            return GetCurrent();

        }

        //next model on the list
        public void Next()
        {
            GetCurrent().model.SetActive(false);
            current++;

            if (current == character.Count)
            {
                current = 0;
            }

            GetCurrent().model.SetActive(true);
        }

        //last model on the list
        public void Prior()
        {
            GetCurrent().model.SetActive(false);
            current--;

            if (current == -1)
            {
                current = character.Count - 1;
            }

            GetCurrent().model.SetActive(true);
        }
    }


    private GameManager gameManager;
    private GamePadManager gamePadManager;

    public Material highLightMaterial;

    [HideInInspector]
    public string playerTag;

    public GameObject joinPanel;
    public Image charImage;

    [HideInInspector]
    //[Range(1, 4)]
    public int playerNum;
    //[HideInInspector]
    public bool selected = false;


    //public List<CharacterEntity> charEntity;
    public CharacterList charEntity;

    [Range(0.01f, 0.30f)]
    public float gamepadReadTime;
    float nextTimeReading;
    float charSelectionDelay;



    //private bool fisrt_frame = true;

    // Use this for initialization
    void Start()
    {

        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        gamePadManager = GameObject.FindGameObjectWithTag("GamePadManager").GetComponent<GamePadManager>();


        // charImage.sprite = charEntity.getCurrent().image;

        nextTimeReading = Time.time;

        charSelectionDelay = Time.time + 1.0f;


    }

    // Update is called once per frame
    void Update()
    {
        //x++;


        //Check if the player is not set
        if (playerTag == null)
        {

            //Get the player
            playerTag = gamePadManager.getPlayerTag(playerNum);

            //No Tag, Assume that there`s no player to select
            selected = true;

            //Got the player.Remove panel
            if (playerTag != null)
            {
                joinPanel.SetActive(false);

                //charImage.sprite = charEntity.getCurrent().image;

                //There`s player to select
                selected = false;
            }

        }
        else
        {

            if (Time.time > nextTimeReading)
            {
                nextTimeReading = Time.time + gamepadReadTime;

                if (!selected)
                {

                    //Debug.Log(Input.GetAxis("joystick 1 Vertical"));
                    if (Input.GetAxis(playerTag + " Vertical") > 0.4)
                    {
                        // if (Input.GetAxis("joystick 1 Vertical") > 0){
                        //charImage.sprite = charEntity.getNext().image;
                        charEntity.Prior();
                    }
                    else if (Input.GetAxis(playerTag + " Vertical") < -0.4)
                    {
                        //charImage.sprite = charEntity.getPrior().image;
                        charEntity.Next();
                    }

                }

            }


            //if (fisrt_frame)
            //{
            //    fisrt_frame = false;
            //}
            //else
            //{
            if (Time.time > charSelectionDelay)
            {
                if (Input.GetButtonDown(playerTag + " PickPushItem"))
                {
                    gameManager.SetPlayer(playerNum, charEntity.GetCurrent().charGO);//, highLightMaterial);

                    selected = true;
                }

                if (Input.GetButtonDown(playerTag + " Cancel"))
                {
                    gameManager.SetPlayer(playerNum, null);//, null);

                    selected = false;
                }
            }
            //}

            //}

        }
    }
}
