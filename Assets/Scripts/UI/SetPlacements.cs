﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Set all the placements, and spawn avatar winner

****************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetPlacements : MonoBehaviour
{


    GameObject model = null;

    [System.Serializable]
    public class Leaderboard
    {

        public Leaderboard(string _name, float _score, GameObject _character)
        {
            score = _score;
            name = _name;
            character = _character;
        }

        public float score;
        public string name;

        public GameObject character;

    }



    public GameObject spawnLocation;
    private GameManager gm;


    public List<Leaderboard> leaderboard;// = new List<Leaderboard>(null, null, null, null);


    public void SpawnWinner()
    {
        Vector3 location = spawnLocation.transform.position;

        //GameObject winner = Instantiate(WinningPlayer , location, new Quaternion()) as GameObject;
    }

    public void SetWinner()
    {
        gm = GetComponent<GameManager>();
    }



    public void AddPlayerPlacement(string name, float score, GameObject model)
    {

        if (leaderboard.Count == 0)
        {

            leaderboard.Add(new Leaderboard(name, score, model));

        }
        else
        {
            int i = 0;
            for (; i < leaderboard.Count; i++)
            {
                if (score > leaderboard[i].score)
                {
                    break;
                }
            }

            leaderboard.Insert(i, new Leaderboard(name, score, model));

        }


        //if (leaderboard.Count == 0)
        //{
        //    leaderboard.Add(new Leaderboard(name, score, null));
        //}

        //for (int i = 0; i < leaderboard.Count; i++)
        //{

        //    //if (leaderboard[i].score >= score)
        //    //{
        //    //    leaderboard.Insert(i, new Leaderboard(name, score, null));
        //    //    //leaderboard[i] = new Leaderboard(name, score, null);
        //    //}
        //    if (score >= leaderboard[i].score)
        //    {
        //        leaderboard.Insert(i, new Leaderboard(name, score, null));
        //    }

        //}




    }

    public static SetPlacements instance;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    private void Update()
    {

        if (SceneManager.GetActiveScene().name == "WinnerScreen")
        {

            HUDLeaderBoard hud = GameObject.FindGameObjectWithTag("HUDLeaderboard").GetComponent<HUDLeaderBoard>();

            if (hud)
            {


                for (int i = 0; i < leaderboard.Count; i++)
                {

                    //if (leaderboard[i].name == "")
                    //{
                        hud.texts[i].panel.SetActive(true);
                    //    continue;
                    //}

                    hud.texts[i].name.text = leaderboard[i].name;
                    hud.texts[i].score.text = leaderboard[i].score.ToString();

                    if (i == 0 && model == null)
                    {
                        Vector3 location = GameObject.FindGameObjectWithTag("WinnerLocation").transform.position;

                        model = Instantiate(leaderboard[i].character, location, new Quaternion()) as GameObject;
                        model.SetActive(true);
                    }

                }


                //if (leaderboard[0] != null)
                //{
                //    hud.winnerName.text = leaderboard[0].name;
                //    hud.winnerScore.text = leaderboard[0].score.ToString();

                //    if (leaderboard[0].character != null)
                //        Instantiate(leaderboard[0].character, GameObject.FindGameObjectWithTag("WinnerLocation").transform.position, new Quaternion());
                //}

                //if (leaderboard[1] != null)
                //{
                //    hud.runnerUpName.text = leaderboard[1].name;
                //    hud.runnerUpScore.text = leaderboard[1].score.ToString();
                //}

                //if (leaderboard[2] != null)
                //{
                //    hud.thirdName.text = leaderboard[2].name;
                //    hud.thirdScore.text = leaderboard[2].score.ToString();
                //}

                //if (leaderboard[3] != null)
                //{
                //    hud.fourthName.text = leaderboard[3].name;
                //    hud.fourthScore.text = leaderboard[3].score.ToString();
                //}

                // Stop script so it does not keep instantiating multiples objects.
                //gameObject.SetActive(false);
                Destroy(gameObject);
            }

        }

    }

}
