﻿/****************************************************

Programmers: Bruno Brito, Eric Sellitto


Function: Checks if the characters are all selected 
and manages them

****************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSelectionCheck : MonoBehaviour
{

    public GameObject button;
    public GameObject eventSystem;


    public List<CharSelection> players;

    public GameManager gameManager;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        bool startGame = true;

        short numplayers = 0;

        for (int i = 0; i < players.Count; i++)
        {

            if (players[i].gameObject.activeSelf)
            {
                startGame = players[i].selected;
                numplayers++;
            }

            if (!startGame)
                break;
        }


        if (startGame & numplayers > 1)
        {
            button.SetActive(true);
        }
        else
        {
            button.SetActive(false);
        }

    }


    public void StartGame()
    {
        gameManager.StartGame();
    }


}
